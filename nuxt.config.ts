// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  components: true,

  modules: [
    '@ant-design-vue/nuxt',
    [
      '@pinia/nuxt',
      {
          autoImports: ['defineStore', 'acceptHMRUpdate'],
      },
  ],
  ],
  imports: {
    dirs: [
      'stores/**',
      'interfaces/**'
    ],
  },
  css: [
    '~/assets/scss/main.scss',
],
})
