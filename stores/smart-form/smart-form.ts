
import { defineStore } from "pinia";

export const useSmartForm = () =>
    defineStore(`smart-form`, () => {
        const formatIdx = ref(null)
        const selectFormat = ref([])
        const formValue = ref([])
        const formKey= reactive({})

        return {
            selectFormat,
            formKey,
            formatIdx,
            formValue
        };
    })();
