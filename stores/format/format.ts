
import { defineStore } from "pinia";

export const useFormat = () =>
  defineStore(`format`, () => {

    const formFormatList = ref([])
    const form =ref({})

    return {
      formFormatList,
      form
    };
  })();
