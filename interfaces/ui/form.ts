export interface Form {
    id:  number
    username: string;
    middlename: string;
    name: string;
    delivery: boolean;
    type: string[];
    resource: string;
    desc: string;
    region: string;
    district: string;
    date: string;
    func: string;

}
